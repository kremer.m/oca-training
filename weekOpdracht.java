import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class weekOpdracht {
   Scanner sc = new Scanner(System.in);
   public static void main(String[] arg){
      System.out.println("Welkom bij Black Jack");
      System.out.println("De eerste letter staat voor soort kaart: H Harten | R Ruiten | S Schoppen | K Klaver");
      System.out.println("De tweede letter na het - staat voor welke kaart het is: 2 tot 9 | B Boer | V Vrouw | H Heer | A Aas");
      try{
         new BlackJack().Spelen();
      }catch (Exception e){
         System.out.println("Bedankt voor het spelen");
      }

   }

   public void Spelen() throws Exception{
      System.out.print("Wat is je naam: ");
      String naamSpeler = sc.nextLine();
      for (; true; ) {
         Stapel stapel = new Stapel();
         stapel.maakGeschuddeStapel();
         System.out.println("");
         Speler speler = new Speler(naamSpeler);
         Speler bank = new Speler("Bank");
         speler.ontvangKaart(stapel.geefKaart());
         bank.ontvangKaart(stapel.geefKaart());
         speler.ontvangKaart(stapel.geefKaart());
         bank.toonHand();
         speler.toonHand();
         bank.ontvangKaart(stapel.geefKaart());

         System.out.print("geef k voor een kaart, q om te stoppen of andere toets om te passen: ");
         String invoer = sc.nextLine();
         if (invoer.equals("q")) {
            throw new Exception();
         }
         if (invoer.equals("k")) {
            speler.ontvangKaart(stapel.geefKaart());
         }
         bank.toonHand();
         speler.toonHand();

         if(speler.waardeHand()<22 && speler.waardeHand()>bank.waardeHand()) {
            if(speler.waardeHand()==21) System.out.println("!!!BLACKJACK!!!");
            System.out.println(speler.Naam + " HEEFT GEWONNEN!!!");
         }
         else
            System.out.println("Helaas, u heeft verloren");

         stapel.toonGeschuddeStapel();

         System.out.print("Geef q om te stoppen of een andere toets om te spelen: ");
         invoer = sc.nextLine();
         if (invoer.equals("q"))
            throw new Exception();

         System.out.print("======================================================================================");
      }
   }
}


class Kaart {
   String soort;
   int waarde;

   Kaart(String opgegevenSoort, int opgegevenWaarde){
      soort = opgegevenSoort;
      waarde = opgegevenWaarde;
   }
}

class Stapel {
   ArrayList<Kaart> stapel = new ArrayList<>(52);
   ArrayList<Kaart> geschuddeStapel = new ArrayList<>(52);
   void maakStapel(){
      maakStapelPerSoort("H");
      maakStapelPerSoort("R");
      maakStapelPerSoort("S");
      maakStapelPerSoort("K");
   }

   void maakStapelPerSoort(String soort){
      for(int a = 0; a < 1; a++){
         for(int b = 2; b < 11; b++){
            stapel.add(new Kaart(soort + "-" + b, b));
         }
         stapel.add(new Kaart(soort + "-B",10));
         stapel.add(new Kaart(soort + "-V",10));
         stapel.add(new Kaart(soort + "-H",10));
         stapel.add(new Kaart(soort + "-A",11));
      }
   }

   void maakGeschuddeStapel(){
      maakStapel();

      while (stapel.size() > 0){
         Random r = new Random();
         int kaartNummer = r.nextInt(stapel.size());
         geschuddeStapel.add(stapel.get(kaartNummer));
         stapel.remove(kaartNummer);
      }
   }

   void toonGeschuddeStapel(){
      System.out.println("");
      System.out.println("Aantal kaarten in de geschuddestapel: " + geschuddeStapel.size() + ". De volgende kaarten zitten erin");
      for( int i = 0; i < geschuddeStapel.size() ; i++){
         System.out.print(geschuddeStapel.get(i).soort + " ");
         if(i==(geschuddeStapel.size()/2)-1){System.out.println("");};
      }
      System.out.println("");
   }

   Kaart geefKaart(){
      Random r = new Random();
      int kaartNummer = r.nextInt(geschuddeStapel.size());
      Kaart tijdelijkeKaart = geschuddeStapel.get(kaartNummer);
      geschuddeStapel.remove(kaartNummer);
      return tijdelijkeKaart;
   }

}

class Speler{
   String Naam;

   ArrayList<Kaart> kaartenInHand = new ArrayList<Kaart>(5);
   Speler(String opgegevenNaam){
      Naam = opgegevenNaam;
   }
   void ontvangKaart(Kaart ontvangenKaart){
      kaartenInHand.add(ontvangenKaart);
   }

   void toonHand(){
      System.out.print(Naam + " heeft de kaarten: ");
      for( int i = 0; i < kaartenInHand.size() ; i++){
         System.out.print(kaartenInHand.get(i).soort + " ");

      }
      System.out.println(". Waarde is: " + waardeHand());
   }

   int waardeHand(){
      int aantalPunten = 0 ;
      int aantalPuntenAasIsEen = 0;
      for( int i = 0; i < kaartenInHand.size() ; i++){
         aantalPunten += kaartenInHand.get(i).waarde;
         if(kaartenInHand.get(i).waarde == 11){
            aantalPuntenAasIsEen += 1;
         } else {
            aantalPuntenAasIsEen += kaartenInHand.get(i).waarde;
         }
      }
      if(aantalPunten>21){
         return aantalPuntenAasIsEen;
      } else{
         return aantalPunten;
      }

   }

}
